<?php 

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

require_once '../includes/DbOperation.php';

function isTheseParametersAvailable($params){
   $available = true; 
   $missingparams = ""; 

   foreach($params as $param){
      if(!isset($_POST[$param]) || strlen($_POST[$param])<=0){
         $available = false; 
         $missingparams = $missingparams . ", " . $param; 
      }
   }

   if(!$available){
      $response = array(); 
      $response['error'] = true; 
      $response['message'] = 'Parameters ' . substr($missingparams, 1, strlen($missingparams)) . ' missing';

      echo json_encode($response);

      die();
   }
}

$response = array();

if(isset($_GET['apicall'])){
   switch($_GET['apicall']){

   //GET DOCTORS
   case 'getdoctors':
      $db = new DbOperation();
      $response['error'] = false; 
      $response['message'] = 'Request successfully completed';
      $response['doctors'] = $db->getDoctors();
   break; 

   //UPDATE DOCTOR
   case 'updatedoctor':
      isTheseParametersAvailable(array('id','ktp','npa','full_name','gender','phone_number','email','specialist','verification_status','address','username','is_active','public_id'));

      $db = new DbOperation();

      $result = $db->updateDoctor(
      $_POST['id'],
      $_POST['ktp'],
      $_POST['npa'],
      $_POST['full_name'],
      $_POST['gender'],
      $_POST['phone_number'],
      $_POST['email'],
      $_POST['specialist'],
      $_POST['verification_status'],
      $_POST['address'],
      $_POST['username'],
      $_POST['is_active'],
      $_POST['public_id'],
      );

      if($result){
         $response['error'] = false; 
         $response['message'] = 'Akun berhasil diperbarui';
         $response['user'] = $db->getDoctorById($_POST['id']);
      }else{
         $response['error'] = true; 
         $response['message'] = 'Some error occurred please try again';
      }

   break; 

   //UPDATE PASSWORD
   case 'updatepassword':
      isTheseParametersAvailable(array('id','password'));

      $db = new DbOperation();

      $result = $db->updatePassword(
      $_POST['id'],
      md5($_POST['password'])
      );

      if($result){
         $response['error'] = false; 
         $response['message'] = 'Password berhasil diperbarui';
      }else{
         $response['error'] = true; 
         $response['message'] = 'Some error occurred please try again';
      }

   break; 

   case 'getdoctorbyid':
      $db = new DbOperation();
      if(isset($_GET['id'])){
         $db = new DbOperation();
         if($db->getDoctorById($_GET['id'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getDoctorById($_GET['id']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }else{
         $response['error'] = true; 
         $response['message'] = 'Nothing to get, provide a params please';
      }
   break; 

   }

}else{
$response['error'] = true; 
$response['message'] = 'Invalid API Call';
}

echo json_encode($response);