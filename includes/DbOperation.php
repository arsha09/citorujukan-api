<?php
 
class DbOperation
{
    private $con;
 
    function __construct()
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
 
        $db = new DbConnect();
 
        $this->con = $db->connect();
    }
 
function getDoctors(){
    $stmt = $this->con->prepare("SELECT * FROM doctor");
    $stmt->execute();
    $stmt->bind_result($id, $ktp, $npa, $full_name, $gender, $phone_number, $email, $specialist, $verification_status, $address, $username, $password, $is_active, $public_id);

    $doctors = array(); 

    while($stmt->fetch()){
    $doctor  = array();
    $doctor['id'] = $id; 
    $doctor['ktp'] = $ktp; 
    $doctor['npa'] = $npa; 
    $doctor['full_name'] = $full_name; 
    $doctor['gender'] = $gender; 
    $doctor['phone_number'] = $phone_number; 
    $doctor['email'] = $email; 
    $doctor['specialist'] = $specialist; 
    $doctor['verification_status'] = $verification_status; 
    $doctor['address'] = $address; 
    $doctor['is_active'] = $is_active; 
    $doctor['public_id'] = $public_id; 

    array_push($doctors, $doctor); 
    }

    return $doctors; 
}

function getDoctorById($id){
    $stmt = $this->con->prepare("SELECT * FROM doctor WHERE id = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->bind_result($id, $ktp, $npa, $full_name, $gender, $phone_number, $email, $specialist, $verification_status, $address, $username, $password, $is_active, $public_id);

    $doctors = array(); 

    while($stmt->fetch()){
    $doctor  = array();
    $doctor['id'] = $id; 
    $doctor['ktp'] = $ktp; 
    $doctor['npa'] = $npa; 
    $doctor['full_name'] = $full_name; 
    $doctor['gender'] = $gender; 
    $doctor['phone_number'] = $phone_number; 
    $doctor['email'] = $email; 
    $doctor['specialist'] = $specialist; 
    $doctor['verification_status'] = $verification_status; 
    $doctor['address'] = $address; 
    $doctor['is_active'] = $is_active; 
    $doctor['public_id'] = $public_id; 

    array_push($doctors, $doctor); 
    }

    return $doctors; 
}

function updateDoctor($id, $ktp, $npa, $full_name, $gender, $phone_number, $email, $specialist, $verification_status, $address, $username, $is_active, $public_id) {
    $stmt = $this->con->prepare("UPDATE doctor SET ktp = ?, npa = ?, full_name = ?, gender = ?, phone_number = ?, email = ?, specialist = ?, verification_status = ?, address = ?, username = ?, is_active = ?, public_id = ? WHERE id = ?");
    $stmt->bind_param("ssssssssssssi", $ktp, $npa, $full_name, $gender, $phone_number, $email, $specialist, $verification_status, $address, $username, $is_active, $public_id, $id);
    if($stmt->execute())
    return true; 
    return false; 
}

function updatePassword($id, $password) {
    $stmt = $this->con->prepare("UPDATE doctor SET password = ? WHERE id = ?");
    $stmt->bind_param("si", $password, $id);
    if($stmt->execute())
    return true; 
    return false; 
}

function createPatient($rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status){
    $stmt = $this->con->prepare("INSERT INTO patient (rm_number, ktp, full_name, email, phone_number, address, birth_date, gender, create_at, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("ssssssssss", $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);
    if($stmt->execute())
    return true; 
    return false; 
}

function getPatients(){
    $stmt = $this->con->prepare("SELECT * FROM patient");
    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); 
    }

    return $patients; 
}

function searchPatient($ktp){
    $stmt = $this->con->prepare("SELECT * FROM patient WHERE ktp = ? ");
    $stmt->bind_param("s", $ktp);

    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); }

    return $patients; 
    
}


function searchPatientRm($rm_number){
    $stmt = $this->con->prepare("SELECT * FROM patient WHERE rm_number = ? ");
    $stmt->bind_param("s", $rm_number);

    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); }

    return $patients; 
    
}

function getPatientsByDate($create_at){
    $stmt = $this->con->prepare("SELECT * FROM patient WHERE DATE(create_at) = ? ");
    $stmt->bind_param("s", $create_at);

    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); }

    return $patients; 
    
}

function getPatientsByDateRange($start, $end){
    $stmt = $this->con->prepare("SELECT * FROM patient WHERE DATE(create_at) BETWEEN ? AND ? ");
    $stmt->bind_param("ss", $start, $end);

    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); }

    return $patients; 
    
}

function getPatientsByStatus($status){
    $stmt = $this->con->prepare("SELECT * FROM patient WHERE status = ? ");
    $stmt->bind_param("s", $status);

    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); }

    return $patients; 
    
}

function getPatientsByDateStatus($create_at, $status){
    $stmt = $this->con->prepare("SELECT * FROM patient WHERE DATE(create_at) = ? AND status = ? ");
    $stmt->bind_param("ss", $create_at, $status);

    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); }

    return $patients; 
    
}

function getPatientsByDateRangeStatus($start, $end, $status){
    $stmt = $this->con->prepare("SELECT * FROM patient WHERE DATE(create_at) BETWEEN ? AND ?  AND status = ?");
    $stmt->bind_param("sss", $start, $end, $status);

    $stmt->execute();
    $stmt->bind_result($id, $rm_number, $ktp, $full_name, $email, $phone_number, $address, $birth_date, $gender, $create_at, $status);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['id'] = $id; 
    $patient['rm_number'] = $rm_number; 
    $patient['ktp'] = $ktp; 
    $patient['full_name'] = $full_name; 
    $patient['email'] = $email; 
    $patient['phone_number'] = $phone_number; 
    $patient['address'] = $address; 
    $patient['birth_date'] = $birth_date; 
    $patient['gender'] = $gender; 
    $patient['create_at'] = $create_at; 
    $patient['status'] = $status; 

    array_push($patients, $patient); }

    return $patients; 
    
}

function getCountPatientsDate($create_at){
    $stmt = $this->con->prepare("SELECT COUNT(*) AS jml_pasien FROM patient WHERE DATE(create_at) = ? ");
    $stmt->bind_param("s", $create_at);
    $stmt->execute();
    $stmt->bind_result($jml_pasien);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['count'] = $jml_pasien; 

    array_push($patients, $patient); }

    return $patients; 
}

function getCountPatients($create_at, $status){
    $stmt = $this->con->prepare("SELECT COUNT(*) AS jml_pasien FROM patient WHERE DATE(create_at) = ? AND status = ? ");
    $stmt->bind_param("ss", $create_at, $status);
    $stmt->execute();
    $stmt->bind_result($jml_pasien);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['count'] = $jml_pasien;

    array_push($patients, $patient); }

    return $patients; 
}

function getCountPatientsDateRange($start, $end){
    $stmt = $this->con->prepare("SELECT COUNT(*) AS jml_pasien FROM patient WHERE DATE(create_at) BETWEEN ? AND ? ");
    $stmt->bind_param("ss", $start, $end);
    $stmt->execute();
    $stmt->bind_result($jml_pasien);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['count'] = $jml_pasien; 

    array_push($patients, $patient); }

    return $patients; 
}

function getCountPatientsDateRangeStatus($start, $end, $status){
    $stmt = $this->con->prepare("SELECT COUNT(*) AS jml_pasien FROM patient WHERE DATE(create_at) BETWEEN ? AND ?  AND status = ? ");
    $stmt->bind_param("sss", $start, $end, $status);
    $stmt->execute();
    $stmt->bind_result($jml_pasien);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['count'] = $jml_pasien;

    array_push($patients, $patient); }

    return $patients; 
}

function getCountPatientsAll(){
    $stmt = $this->con->prepare("SELECT COUNT(*) AS jml_pasien FROM patient");
    $stmt->execute();
    $stmt->bind_result($jml_pasien);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['count'] = $jml_pasien;

    array_push($patients, $patient); }

    return $patients; 
}

function getCountPatientsAllStatus($status){
    $stmt = $this->con->prepare("SELECT COUNT(*) AS jml_pasien FROM patient WHERE status = ? ");
    $stmt->bind_param("s", $status);
    $stmt->execute();
    $stmt->bind_result($jml_pasien);

    $patients = array(); 

    while($stmt->fetch()){
    $patient  = array();
    $patient['count'] = $jml_pasien;

    array_push($patients, $patient); }

    return $patients; 
}

    
}