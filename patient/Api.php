<?php 

require_once '../includes/DbOperation.php';

function isTheseParametersAvailable($params){
   $available = true; 
   $missingparams = ""; 

   foreach($params as $param){
      if(!isset($_POST[$param]) || strlen($_POST[$param])<=0){
         $available = false; 
         $missingparams = $missingparams . ", " . $param; 
      }
   }

   if(!$available){
      $response = array(); 
      $response['error'] = true; 
      $response['message'] = 'Parameters ' . substr($missingparams, 1, strlen($missingparams)) . ' missing';

      echo json_encode($response);

      die();
   }
}

$response = array();

if(isset($_GET['apicall'])){
   switch($_GET['apicall']){

   //CREATE PATIENT
   case 'createpatient':
      isTheseParametersAvailable(array('ktp','full_name','email','phone_number','address','birth_date','gender'));

      $db = new DbOperation();

      $result = $db->createPatient(
      '10239874231',
      $_POST['ktp'],
      $_POST['full_name'],
      $_POST['email'],
      $_POST['phone_number'],
      $_POST['address'],
      $_POST['birth_date'],
      $_POST['gender'],
      date("Y-m-d H:i:s"),
      '0'
      );

      if($result){
         $response['error'] = false; 
         $response['message'] = 'Patient addedd successfully';
         $response['patients'] = $db->getPatients();
      }else{
         $response['error'] = true; 
         $response['message'] = 'Some error occurred please try again';
      }

   break; 

   //GET PATIENTS
   case 'getpatients':
      $db = new DbOperation();
      $response['error'] = false; 
      $response['message'] = 'Request successfully completed';
      $response['patients'] = $db->getPatients();
   break; 

   //SELECT PATIENT BY...
   case 'selectpatient':
      $db = new DbOperation();
      if(isset($_GET['ktp'])){
         $db = new DbOperation();
         if($db->searchPatient($_GET['ktp'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->searchPatient($_GET['ktp']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }elseif(isset($_GET['rm_number'])){
         $db = new DbOperation();
         if($db->searchPatientRm($_GET['rm_number'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->searchPatientRm($_GET['rm_number']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }elseif(isset($_GET['create_at'])&&isset($_GET['status'])){
         $db = new DbOperation();
         if($db->getPatientsByDateStatus($_GET['create_at'], $_GET['status'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getPatientsByDateStatus($_GET['create_at'], $_GET['status']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }elseif(isset($_GET['create_at'])){
         $db = new DbOperation();
         if($db->getPatientsByDate($_GET['create_at'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getPatientsByDate($_GET['create_at']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }elseif(isset($_GET['start'])&&isset($_GET['end'])&&isset($_GET['status'])){
         $db = new DbOperation();
         if($db->getPatientsByDateRangeStatus($_GET['start'], $_GET['end'], $_GET['status'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getPatientsByDateRangeStatus($_GET['start'], $_GET['end'], $_GET['status']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }elseif(isset($_GET['start'])&&isset($_GET['end'])){
         $db = new DbOperation();
         if($db->getPatientsByDateRange($_GET['start'], $_GET['end'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getPatientsByDateRange($_GET['start'], $_GET['end']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }elseif(isset($_GET['status'])){
         $db = new DbOperation();
         if($db->getPatientsByStatus($_GET['status'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getPatientsByStatus($_GET['status']);
         }else{
            $response['error'] = true; 
            $response['message'] = 'Tidak ada data pasien';
         }
      }else{
         $response['error'] = true; 
         $response['message'] = 'Nothing to get, provide a params please';
      }
   break; 

   //COUNT PATIENT
   case 'countpatients':
      $db = new DbOperation();
      if(isset($_GET['create_at'])&&isset($_GET['status'])){
         $db = new DbOperation();
         if($db->getCountPatients($_GET['create_at'], $_GET['status'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getCountPatients($_GET['create_at'], $_GET['status']);
         }else{
            $response['error'] = true; 
            $response['message'] = '0';
         }
      }elseif(isset($_GET['create_at'])){
         $db = new DbOperation();
         if($db->getCountPatientsDate($_GET['create_at'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getCountPatientsDate($_GET['create_at']);
         }else{
            $response['error'] = true; 
            $response['message'] = '0';
         }
      }elseif(isset($_GET['start'])&&isset($_GET['end'])&&isset($_GET['status'])){
         $db = new DbOperation();
         if($db->getCountPatientsDateRangeStatus($_GET['start'], $_GET['end'], $_GET['status'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getCountPatientsDateRangeStatus($_GET['start'], $_GET['end'], $_GET['status']);
         }else{
            $response['error'] = true; 
            $response['message'] = '0';
         }
      }elseif(isset($_GET['start'])&&isset($_GET['end'])){
         $db = new DbOperation();
         if($db->getCountPatientsDateRange($_GET['start'], $_GET['end'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getCountPatientsDateRange($_GET['start'], $_GET['end']);
         }else{
            $response['error'] = true; 
            $response['message'] = '0';
         }
      }elseif(isset($_GET['status'])){
         $db = new DbOperation();
         if($db->getCountPatientsAllStatus($_GET['status'])){
            $response['error'] = false; 
            $response['message'] = 'Request successfully completed';
            $response['patients'] = $db->getCountPatientsAllStatus($_GET['status']);
         }else{
            $response['error'] = true; 
            $response['message'] = '0';
         }
      }else{
         $response['error'] = false; 
         $response['message'] = 'Request successfully completed';
         $response['patients'] = $db->getCountPatientsAll();
      }
   break; 

   }

}else{
$response['error'] = true; 
$response['message'] = 'Invalid API Call';
}

echo json_encode($response);