-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 07:26 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `citorujukan`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `id` int(11) NOT NULL,
  `ktp` varchar(16) NOT NULL,
  `npa` int(6) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `gender` enum('1','2') NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `specialist` varchar(100) NOT NULL,
  `verification_status` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_active` enum('1','2') NOT NULL,
  `public_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `ktp`, `npa`, `full_name`, `gender`, `phone_number`, `email`, `specialist`, `verification_status`, `address`, `username`, `password`, `is_active`, `public_id`) VALUES
(1, '3374101122000006', 123456, 'Arsha Trikusuma', '1', '083838863174', 'arsha@labcito.com', 'Umum', '1', 'Jl. Beringin No. 129 Semarang', 'arsha', '50a18e391a4137b2aefb5afe93a498ae', '1', 'abc123'),
(2, '3374007766550002', 98253, 'Ferry Gunawan', '1', '086723845109', 'ferrygun@gmail.com', 'Kandungan', '1', 'Jl. telaga Bodas Raya No. 112 Semarang', 'ferrygun', 'aec9ad1b08746d5107134d4ff872ff58', '1', 'cde098'),
(3, '3374003388550003', 542987, 'Erik Pangalila', '1', '086112098342', 'erikpa@gmail.com', 'THT', '1', 'Jl. Sunan Muria No. 12 Kudus', 'erik', 'f12537e9605b2b1bf3122bb12a0e24f7', '1', 'efg542'),
(4, '3374001144220004', 671003, 'Prihastuti Ningsih', '2', '086990231009', 'priningsih@gmail.com', 'Pernafasan', '1', 'Jl. Perintis Kemerdekaan No 10 Jakarta', 'priningsih', '977d2f0d42e75097448b1fd9d7b82fb1', '1', 'erp001'),
(6, '3374001299330004', 324569, 'Isti Prihastuti', '2', '086990321009', 'isti@gmail.com', 'Dokter Umum', '1', 'Jl. Arjuna No. 33 Semarang', 'isti', 'f254ae19b2a728285f62a3c10a7fe656', '1', 'erp001');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `rm_number` varchar(11) NOT NULL,
  `ktp` varchar(16) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `address` varchar(200) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` enum('1','2') DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `rm_number`, `ktp`, `full_name`, `email`, `phone_number`, `address`, `birth_date`, `gender`, `create_at`, `status`) VALUES
(1, '12345678901', '3374009988770001', 'Richard Simbolon', 'richardsim@gmail.com', '086777324123', 'Jl. Pawiyatan Luhur No. 114 Semarang', '1997-07-04', '1', '2022-07-05 16:02:03', '0'),
(2, '90876538902', '3374009911000004', 'Erwan Nirwanto', 'erwan@gmail.com', '086745231098', 'Jl. Wrekudoro No. 12 Semarang', '1998-09-22', '1', '2022-07-05 16:02:03', '0'),
(5, '10239874231', '3374002309780009', 'Bagus Pradianto', 'bagusp@gmail.com', '086123098345', 'Jl. Ahmad Yani No. 110 Banyumas', '2003-04-12', '1', '2022-07-05 16:02:03', '1'),
(7, '10239874231', '3374005634890002', 'Istiani Wulandari', 'isti@gmail.com', '086902134564', 'Jl. Wahid Hasyim No. 120 Kendal', '2002-09-12', '2', '2022-07-05 16:02:03', '0'),
(8, '10239874231', '3374001234560001', 'Eko Pradianto', 'preko@gmail.com', '086453123908', 'Jl. Sawojajar No. 90 Jepara', '1994-05-23', '1', '2022-07-05 16:02:03', '0'),
(9, '10239874231', '3374003322770008', 'Reni Fadhila', 'renifd@gmail.com', '086990234156', 'Jl. Sawojajar No. 98 Rembang', '2000-09-14', '2', '2022-07-05 11:17:22', '0'),
(10, '10239874231', '3374009834560005', 'Esa Santoso', 'esasantoso@gmail.com', '086789324109', 'Jl. Arya Penangsang No. 90 Kutaraja', '2001-09-07', '1', '2022-07-06 04:39:08', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ktp` (`ktp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
