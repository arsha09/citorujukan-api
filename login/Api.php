<?php 
//getting the database connection
 require_once 'DbConnect.php';
 
 $response = array();
 
 if(isset($_GET['apicall'])){
 
 switch($_GET['apicall']){
 
 case 'signup':
 if(isTheseParametersAvailable(array('ktp','npa','full_name','gender','phone_number','email','specialist','address','username','password'))){
 
    //getting the values 
    $ktp = $_POST['ktp'];
    $npa = $_POST['npa'];
    $full_name = $_POST['full_name'];
    $gender = $_POST['gender'];
    $phone_number = $_POST['phone_number'];
    $email = $_POST['email']; 
    $specialist = $_POST['specialist']; 
    $verification_status = '1'; 
    $address = $_POST['address']; 
    $username = $_POST['username']; 
    $password = md5($_POST['password']);
    $is_active = '1';
    $public_id = 'erp001';
    
    $stmt = $conn->prepare("SELECT id FROM doctor WHERE ktp = ? OR npa = ? OR username = ? OR email = ?");
    $stmt->bind_param("ssss", $ktp, $npa, $username, $email);
    $stmt->execute();
    $stmt->store_result();
    
    //if the user already exist in the database 
    if($stmt->num_rows > 0){
    $response['error'] = true;
    $response['message'] = 'User already registered';
    $stmt->close();
    }else{
    
    //if user is new creating an insert query 
    $stmt = $conn->prepare("INSERT INTO doctor (ktp, npa, full_name, gender, phone_number, email, specialist, verification_status, address, username, password, is_active, public_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssssssssssss", $ktp, $npa, $full_name, $gender, $phone_number, $email, $specialist, $verification_status, $address, $username, $password, $is_active, $public_id);
    
    //if the user is successfully added to the database 
    if($stmt->execute()){
    
    //fetching the user back 
    $stmt = $conn->prepare("SELECT id, id, ktp, npa, full_name, gender, phone_number, email, specialist, verification_status, address, username, is_active, public_id FROM doctor WHERE username = ?"); 
    $stmt->bind_param("s",$username);
    $stmt->execute();
    $stmt->bind_result($userid, $id, $ktp, $npa, $full_name, $gender, $phone_number, $email, $specialist, $verification_status, $address, $username, $is_active, $public_id);
    $stmt->fetch();
    
    $user = array(
    'id'=>$id, 
    'ktp'=>$ktp, 
    'npa'=>$npa, 
    'full_name'=>$full_name,
    'gender'=>$gender, 
    'phone_number'=>$phone_number,
    'email'=>$email,
    'specialist'=>$specialist,
    'verification_status'=>$verification_status,
    'address'=>$address, 
    'username'=>$username, 
    'is_active'=>$is_active,
    'public_id'=>$public_id
    );
    
    $stmt->close();
    
    //adding the user data in response 
    $response['error'] = false; 
    $response['message'] = 'User registered successfully'; 
    $response['user'] = $user; 
    }
    }
    
    }else{
    $response['error'] = true; 
    $response['message'] = 'required parameters are not available'; 
    }
 
 break; 
 
 case 'login':
 
 //this part will handle the login 
 //for login we need the username and password 
 if(isTheseParametersAvailable(array('username', 'password'))){
    //getting values 
    $username = $_POST['username'];
    $password = md5($_POST['password']); 
    
    //creating the query 
    $stmt = $conn->prepare("SELECT id, ktp, npa, full_name, gender, phone_number, email, specialist, verification_status, address, username, is_active, public_id FROM doctor WHERE username = ? AND password = ?");
    $stmt->bind_param("ss", $username, $password);
    
    $stmt->execute();
    
    $stmt->store_result();
    
    //if the user exist with given credentials 
    if($stmt->num_rows > 0){
    
    $stmt->bind_result($id, $ktp, $npa, $full_name, $gender, $phone_number, $email, $specialist, $verification_status, $address, $username, $is_active, $public_id);
    $stmt->fetch();
    
    $user = array(
    'id'=>$id, 
    'ktp'=>$ktp, 
    'npa'=>$npa, 
    'full_name'=>$full_name,
    'gender'=>$gender, 
    'phone_number'=>$phone_number,
    'email'=>$email,
    'specialist'=>$specialist,
    'verification_status'=>$verification_status,
    'address'=>$address, 
    'username'=>$username, 
    'is_active'=>$is_active,
    'public_id'=>$public_id
    );
    
    $response['error'] = false; 
    $response['message'] = 'Login successfull'; 
    $response['user'] = $user; 
    }else{
    //if the user not found 
    $response['error'] = false; 
    $response['message'] = 'Invalid username or password';
    }
    }
 
 break; 
 
 default: 
 $response['error'] = true; 
 $response['message'] = 'Invalid Operation Called';
 }
 
 }else{
 //if it is not api call 
 //pushing appropriate values to response array 
 $response['error'] = true; 
 $response['message'] = 'Invalid API Call';
 }
 
 //displaying the response in json structure 
 echo json_encode($response);

 //function validating all the paramters are available
 //we will pass the required parameters to this function 
 function isTheseParametersAvailable($params){
 
    //traversing through all the parameters 
    foreach($params as $param){
    //if the paramter is not available
    if(!isset($_POST[$param])){
    //return false 
    return false; 
    }
    }
    //return true if every param is available 
    return true; 
    }